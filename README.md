# Python Documentor for Formal Web

Python Documentor generates markdown documentation for Python modules.

## Running Documentor

1. Make sure you have [installed `python` and `pip`][pip]. 
2. [Install `pipenv`](https://docs.pipenv.org/install/)
to download the required dependencies.
3. Run `pipenv install` to install the dependencies and create
a virtual environment, then run `pipenv shell` to enter it.
4. Run `python docs.py` to run the app locally (or just `import docs` in Python)

[pip]: https://packaging.python.org/tutorials/installing-packages/#requirements-for-installing-packages

## Benefits

Markdown is easy to read and to work with on Github.
ReStructured Text is the Python-native markup language,
but it is not as familiar to people outside Python.

Including documentation within source code files is one of
the best ways to encourage developers to keep it up to date.
This is good for developers, but it's exhausting to read through
source files any time you are just looking for a quick reference.
Markdown documentation in the repository should address this simply.

Having documentation in markdown instead of HTML makes
the documentation more lightweight and portable.
It is also readable even when it's not rendered.
You can always still convert the markdown to HTML.

## Drawbacks

This is a slightly non-standard way of doing things.
Developers may be comfortable with more standard tools.
We also lose support of advantages of ReStructured Text
like being able to link to Python objects automatically.

## Possible Alternatives

We could just use [Sphinx](http://www.sphinx-doc.org/),
which is the documentation generator that Python itself uses. 
There are some nice themes and features, but it seems complicated.

## How It Works

If you have a Python module `docs.py` like this...

```python
"""
This module generates documentation.
"""
import sys

def get_docs(module):
    """Get the docstrings of a module."""
    pass

def generate_docs(module, output=sys.stdout):
    """Generate markdown documentation."""
    pass

class Doc:
    """A `Doc` object encapsulates an object."""
    def __init__(self, object):
        self.object = object

    def doc():
        """Get the docstring of the object."""
        return inspect.getdoc(self.object)
```

...running `python docs.py` should generate a markdown file `docs.md` like this:

```markdown
# `docs`

This module generates documentation.

## Table of Contents

- [`Doc`](#doc)
- - [`.doc`](#doc)
- [`get_docs`](#get_docs)
- [`generate_docs`](#generate_docs)

## `Doc`
__`Doc(object)`__

A `Doc` object encapsulates an object.

### `.doc()`
__`.doc()`__

Get the docstring of the object.

## `get_docs`
__`get_docs(module)`__

Get the docstrings of a module.

## `generate_docs`
__`generate_docs(module, output=sys.stdout)`__

Generate markdown documentation.
```
~^^~