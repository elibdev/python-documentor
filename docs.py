"""
This module generates markdown documentation from a Python module.
"""

import importlib
import inspect
import os
import subprocess
from abc import ABC, abstractmethod

class ObjectDocs(ABC):
    """
    `ObjectDocs` is an abstract base class that represents the
    documentation for a given Python object.
    `ObjectDocs` must be subclassed for different types of objects.
    You must provide an implementation for `markdown()`.
    """
    def __init__(self, object):
        self.object = object

    @property
    def name(self):
        """Get the name of the object as a string."""
        return self.object.__name__

    @property
    def docstring(self):
        """Get the docstring of the object."""
        return inspect.getdoc(self.object)

    @property
    def signature(self):
        """Get the signature of the object if it's a callable."""
        return str(inspect.signature(self.object))

    @property
    @abstractmethod
    def markdown(self):
        """Get a string of the markdown representation of the docs."""
        raise NotImplementedError()

class FunctionDocs(ObjectDocs):
    """
    `FunctionDocs` represents the documentation for a function.

    ```python
    >>> import os; f = FunctionDocs(os.fsdecode)
    >>> f.name == "fsdecode"
    True
    >>> f.signature == "(filename)"
    True
    >>> f.docstring[0:6] == "Decode"
    True
    >>> print(f.markdown) # doctest: +ELLIPSIS
    <BLANKLINE>
    ## `fsdecode`
    __`fsdecode(filename)`__
    <BLANKLINE>
    Decode filename...

    ```
    """
    @property
    def markdown(self):
        return f"""
## `{self.name}`
__`{self.name}{self.signature}`__

{self.docstring}
"""

class MethodDocs(ObjectDocs):
    """
    `MethodDocs` represents the documentation for a method.

    ```python
    >>> import queue; m = MethodDocs(queue.Queue.put_nowait)
    >>> m.name
    'put_nowait'
    >>> m.signature == "(self, item)"
    True
    >>> m.docstring[0:3] == "Put"
    True
    >>> print(m.markdown) # doctest: +ELLIPSIS
    <BLANKLINE>
    ### `.put_nowait`
    __`.put_nowait(self, item)`__
    <BLANKLINE>
    Put...

    ```
    """
    @property
    def markdown(self):
        return f"""
### `.{self.name}`
__`.{self.name}{self.signature}`__

{self.docstring}
"""

class ClassDocs(ObjectDocs):
    """
    `ClassDocs` represents the documentation for a class.

    ```python
    >>> import queue; c = ClassDocs(queue.Queue)
    >>> c.name == "Queue"
    True
    >>> c.signature == "(self, maxsize=0)"
    True
    >>> c.docstring[0:14] == "Create a queue"
    True
    >>> print(c.methods_markdown) # doctest: +ELLIPSIS
    <BLANKLINE>
    ### `.empty`...
    >>> print(c.markdown) # doctest: +ELLIPSIS
    <BLANKLINE>
    ## `Queue`
    __`Queue(self, maxsize=0)`__
    <BLANKLINE>
    Create a queue...

    ```
    """
    @property
    def signature(self):
        """Get the signature of the class's `__init__` function."""
        return str(inspect.signature(self.object.__init__))

    @property
    def markdown(self):
        return f"""
## `{self.name}`
__`{self.name}{self.signature}`__

{self.docstring}
{self.methods_markdown}
"""

    @property
    def methods_markdown(self):
        """Get a markdown representation of the class's methods."""
        markdown = ""
        # Methods of a class are still functions until an instance is created.
        methods = inspect.getmembers(self.object, inspect.isfunction)
        for name, method in methods:
            is_public = not name.startswith("_")
            if is_public:
                markdown += MethodDocs(method).markdown
        return markdown

class ModuleDocs(ObjectDocs):
    """
    `ModuleDocs` represents the documentation for a Python module.

    ```python
    >>> import queue; m = ModuleDocs(queue)
    >>> m.name == "queue"
    True
    >>> m.docstring[0:16] == "A multi-producer"
    True
    >>> print(m.markdown) # doctest: +ELLIPSIS
    <BLANKLINE>
    # `queue`
    <BLANKLINE>
    A multi-producer...
    
    ```
    """
    @property
    def markdown(self):
        """
        Return a markdown document with documentation created from the
        docstrings for the functions and classes in the module.
        """
        return f"""
# `{self.name}`

{self.docstring}
{self._markdown_for(inspect.isclass, ClassDocs)}
{self._markdown_for(inspect.isfunction, FunctionDocs)}
"""

    def _markdown_for(self, filter, docs_class):
        """
        Get the markdown representation for all members of the module
        that pass the `filter` using the `docs_class` to build it.
        This is used for getting the docs of a certain type of object.

        - `filter` is a function that takes an object and returns a boolean
        to indicate whether it passes the filter (e.g. `inspect.isclass`).
        - `docs_class` is a subclass of `ObjectDocs` for the type of object.
        """
        markdown = ""
        objects = inspect.getmembers(self.object, filter)
        for name, object in objects:
            is_public = not name.startswith("_")
            is_in_module = (inspect.getmodule(object) == self.object)
            if is_public and is_in_module:
                markdown += docs_class(object).markdown
        return markdown

def write_docs(code, docs):
    """
    Generate markdown documentation in `docs` from `code`.
    `docs` and `code` are paths to directories that should exist.
    This will destructively clear the current contents of `docs`.
    """
    if os.path.exists(docs):
        subprocess.run(["rm", "-r", docs])
    subprocess.run(["mkdir", docs])
    for filename in os.listdir(os.getcwd()):
        if filename.endswith(".py"):
            # Import the module and get its markdown docs.
            m = importlib.import_module(inspect.getmodulename(filename))
            md = ModuleDocs(m).markdown
            # Change file extension from .py to .md
            md_name = os.path.splitext(filename)[0] + ".md"
            md_path = os.path.join(docs, md_name)
            with open(md_path, "w+") as f:
                f.write(md)

if __name__ == "__main__":
    write_docs(os.getcwd(), "../docs")
